#!/usr/bin/env python3

import sys, signal, copy, subprocess, git

import argparse
parser = argparse.ArgumentParser(description='\033[1mUniversal executable to produce n-tuples for high-level analysis with CMS data\033[0m')
parser.add_argument('configFile', metavar='config.json', nargs='?', default="",
                    help='configuration file in JSON format (see example in `test/*.json`)')
parser.add_argument('-t', '--test', help="run locally (for test)", action="store_true")
parser.add_argument('-l', '--local', help="run on local HTCondor (only if data sets on local T2)", action="store_true")
parser.add_argument('-c', '--crab', help="run remotely (for production)", action="store_true")
args = parser.parse_args()

import os, json
if os.path.exists(args.configFile):
    print("Opening " + args.configFile)
    with open(args.configFile, "r") as f:
        params = json.load(f)
else:
    print("Warning: no config file given in argument. The script will be run with a default data set. This probably only makes sense for testing.")
    params = {
                'name': 'test',
                'datasets': { '/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM': {'splitting': 'FileBased'} },
                'maxEvents': 100,
                'flags': { 'options' : ['jets'] }
             }

origin = os.environ['CMSSW_BASE'] + '/src/Core'
repo = git.Repo(origin)
sha = repo.head.object.hexsha[:12]
print(sha)
assert len(sha) == 12

name = (params['name'] + '_') if 'name' in params else ''
datasets = params['datasets'] if 'datasets' in params else {'/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM': {'splitting': 'FileBased'}}
maxEvents = params['maxEvents'] if 'maxEvents' in params else 100
psetName = origin + '/Ntupliser/python/Ntupliser_cfg.py'

certificate = os.popen('voms-proxy-info --timeleft').read()
if certificate[0] == "0":
    sys.stderr.write("\033[31mYou need to activate your certificate\033[0m\n")
    sys.exit(1)

if 'luminosity' in params:
    for key, value in params['luminosity'].items():
        params['luminosity'][key] = os.path.expandvars(value)

if args.test:

    for dataset, any_info in datasets.items():
        print('\033[1m' + dataset + '\033[0m')
        datasetSplit = dataset.split('/')
        if len(datasetSplit) != 4:
            sys.stderr.write("\033[31Data set is not recognised\033[0m\n")
            sys.exit(1)
        cmd = 'dasgoclient --query="file dataset='+dataset+'" -limit=1'
        inputFiles = os.popen(cmd).read().splitlines()
        if len(inputFiles) == 0:
            sys.stderr.write("\033[31mNo input file was found\033[0m\n")
            sys.exit(1)
        inputFile = 'root://cms-xrd-global.cern.ch/' + inputFiles[0]
        outputDir = datasetSplit[1] + '_' + sha
        if not os.path.exists(outputDir):
            os.mkdir(outputDir)
        outputFile = outputDir + '/test_' + name + datasetSplit[2] + ".root"
        configFile = outputDir + '/test_' + name + datasetSplit[2] + ".json"
        print("input: " + inputFile)
        print("output: " + outputFile)
        local_params = copy.deepcopy(params)
        local_params['datasets'] = {dataset : any_info}
        with open(configFile, 'w') as f:
            json.dump(local_params, f, indent = 4)
        cmd = 'cmsRun ' + psetName + ' inputFiles=' + inputFile + ' outputFile=' + outputFile + ' maxEvents=' + str(maxEvents) + ' configFile=' + configFile
        print(cmd)
        wrapped_proc = subprocess.run(cmd, shell=True)

        ## in case some presses Ctrl+C: TODO: fix (not working ATM)
        #def handler(signum, frame):
        #    wrapped_proc.send_signal(signum)
        #    sys.exit()
        #signal.signal(signal.SIGINT, handler)

if not args.local and not args.crab:
    sys.exit(0)

if repo.is_dirty():
    sys.stderr.write("\033[31mCRAB or HTCondor jobs may only be submitted if all changes have been committed\033[0m\n")
    sys.exit(1)

if len(args.configFile) == 0:
    sys.stderr.write("\033[31mExpecting a config file for `-l` and `-c` options\033[0m\n")
    sys.exit(1)


if args.local:

    for dataset, any_info in datasets.items():
        print('\033[1m' + dataset + '\033[0m')
        datasetSplit = dataset.split('/')
        if len(datasetSplit) != 4:
            sys.stderr.write("\033[31mData set is not recognised\033[0m\n")
            sys.exit(1)
        data = datasetSplit[1]
        campaign = datasetSplit[2].split('-')
        aod = datasetSplit[3]
        inputFiles = params['htcondor']['tier2'] + '/' + campaign[0] + '/' + data + '/' + aod + '/' + campaign[1] + '-' + campaign[2] + "/*/*.root"
        from glob import glob
        if len(glob(inputFiles)) == 0:
            sys.stderr.write("\033[31mInput files cannot be found locally\033[0m\n")
            sys.exit(1)

        outputDir = os.environ["PWD"] + '/' + datasetSplit[1] + '_' + sha + '/htc_' + name + datasetSplit[2]
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)

        local_params = copy.deepcopy(params)
        local_params['datasets'] = {dataset : any_info}
        with open(outputDir + '.json', 'w') as f:
            json.dump(local_params, f, indent = 4)
        cmsRun = f'{psetName} inputFiles="{inputFiles}" outputDir={outputDir} configFile={outputDir}.json'
        print(cmsRun)
        condor_submit = f'condor_submit psetName={cmsRun} {os.environ["CMSSW_BASE"]}/src/Core/Ntupliser/test/.condor'
        subprocess.run(condor_submit, check=True, shell=True)

if args.crab:

    if not 'crab' in params:
        params['crab'] = {
                'storageSite': 'T2_CH_CERN'
                }
    crab = params['crab']
    crab['sandbox'] = True # NOTE: if there is already existing way for the `cmsRun`
                           #       to know when it is called to submit CRAB jobs,
                           #       then this variable can be removed

    from CRABAPI.RawCommand import crabCommand
    from WMCore.Configuration import Configuration
    config = Configuration()

    config.section_("General")

    config.section_("JobType")
    config.JobType.pluginName = 'Analysis'
    config.JobType.psetName = psetName
    config.JobType.allowUndistributedCMSSW = True
    config.JobType.maxMemoryMB  = 2500
    config.JobType.sendExternalFolder = True

    config.section_("Data")
    config.Data.unitsPerJob = 200
    config.Data.ignoreLocality = False
    config.Data.runRange = ''
    config.Data.publication = False

    # https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3DataHandling
    config.General.transferOutputs = True
    config.General.transferLogs = True

    config.section_("Site")
    config.Site.storageSite = crab['storageSite']
    print("storage site: "+config.Site.storageSite)

    import multiprocessing
    from multiprocessing import Process

    def submit(config):
        return crabCommand('submit', config = config)

    for dataset, any_info in datasets.items():
        print('\033[1m' + dataset + '\033[0m')

        datasetSplit = dataset.split('/')
        if len(datasetSplit) != 4:
            sys.stderr.write("\033[31Data set is not recognised\033[0m\n")
            sys.exit(1)

        PD = datasetSplit[1]
        output = name + datasetSplit[2]
        local_output = output[:100]
        pnfs_output = sha + '_' + output

        config.General.workArea = PD + '_' + sha
        os.makedirs(config.General.workArea, exist_ok = True)
        config.General.requestName = local_output
        print("local output: " + config.General.workArea + "/crab_" + config.General.requestName)

        config.Data.splitting = any_info['splitting'] if 'splitting' in any_info else 'Automatic'
        config.Data.inputDataset = dataset
        print("input: " + config.Data.inputDataset)

        config.Data.outputDatasetTag = pnfs_output
        if 'luminosity' in params and 'mask' in params['luminosity']:
            config.Data.lumiMask = params['luminosity']['mask']
        print("storage: " + PD + '/' + config.Data.outputDatasetTag)

        local_params = copy.deepcopy(params)
        local_params['datasets'] = {dataset : any_info}
        local_params['crab']
        local_params_output = f"{config.General.workArea}/crab_{config.General.requestName}.json"

        config.JobType.pyCfgParams = ['inputFiles='+dataset,'configFile='+str(local_params_output)]
        config.JobType.inputFiles = [str(local_params_output)]
        if 'luminosity' in params and 'pileup' in params['luminosity']:
            config.JobType.inputFiles += [params['luminosity']['pileup']]
        # List of private input files (and/or directories) needed by the jobs. They will be added to the input sandbox. The input sandbox can not exceed 120 MB. The input sandbox is shipped with each job. The input files will be placed in the working directory where the users' application (e.g. cmsRun) is launched regardless of a possible path indicated in this parameter (i.e. only the file name at right of last / is relevant). Directories are tarred and their subtree structure is preserved. Please check the FAQ for more details on how these files are handled. 

        with open(local_params_output, 'w') as f:
            json.dump(local_params, f, indent = 4)

        p = Process(target=submit,args=(config,))
        p.start()
        p.join()
