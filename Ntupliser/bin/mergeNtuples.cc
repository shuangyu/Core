#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>
#include <algorithm>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"

#include <TFile.h>
#include <TChain.h>
#include <TH1.h>

#include "Core/JetVetoMaps/interface/Conservative.h"
#include "Core/MET/interface/Filters.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Get fractioned *n*-tuples after the n-tuplisation and merge them into a 
/// single file. Complementary information about the pile-up may be included.
/// A minimal selection is applied, e.g. on the primary vertex (PV).
void mergeNtuples
                (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
                 const fs::path& output, //!< output ROOT file (n-tuple)
                 const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                 const int steering, //!< parameters obtained from explicit options 
                 const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs, "ntupliser/events");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    bool isMC = metainfo.Get<bool>("flags", "isMC");
    int year = metainfo.Get<int>("flags", "year");

    // event information
    GenEvent * genEvt = nullptr;
    if (isMC)
        tIn->SetBranchAddress("genEvent", &genEvt);
    RecEvent * recEvt = nullptr;
    tIn->SetBranchAddress("recEvent", &recEvt);
    PrimaryVertex * vtx = nullptr;
    tIn->SetBranchAddress("primaryvertex", &vtx);

    // physics object information
    vector<RecJet> * recJets = nullptr;
    if (branchExists(tIn, "recJets"))
        tIn->SetBranchAddress("recJets", &recJets);
    vector<RecMuon> * recMuons = nullptr;
    if (branchExists(tIn, "recMuons"))
        tIn->SetBranchAddress("recMuons", &recMuons);
    vector<RecPhoton> * recPhotons = nullptr;
    if (branchExists(tIn, "recPhotons"))
        tIn->SetBranchAddress("recPhotons", &recPhotons);

    // jet veto business
    const auto& jetveto_file = config.get<fs::path>("corrections.jetvetomap.filename");
    optional<JetVeto::Conservative> jetveto;
    if (jetveto_file != "/dev/null")
        jetveto = make_optional<JetVeto::Conservative>(jetveto_file);
    else
        cout << orange << "No jet veto file was provided." << def << endl;
    metainfo.Set<fs::path>("corrections", "jetvetomap", "filename", jetveto_file);

    // MET filter business
    MET * met = nullptr;
    optional<MissingET::Filters> metfilters;
    bool applyMETfilters = config.get<bool>("corrections.METfilters");
    if (applyMETfilters) {
        metfilters = make_optional<MissingET::Filters>(year);
        tIn->SetBranchAddress("met", &met);
    }
    else
        cout << orange << "No MET filters will be applied." << def << endl;
    metainfo.Set<bool>("corrections", "METfilters", applyMETfilters);

    // dummy thread-safe histogram, used especially in MC for normalisation
    TH1 * hSumWgt = nullptr;
    if (isMC) hSumWgt = new TH1F("hSumWgt",";;#sum_{i} w_{i}", 1, -0.5, 0.5);

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        const bool badPV = abs(vtx->z) > 24 /* cm */ || abs(vtx->Rho) > 2 /* cm */ || vtx->fake;
        if (badPV) {
            recEvt->weights *= 0;
            if (recJets) recJets->clear();
            if (recMuons) recMuons->clear();
            if (recPhotons) recPhotons->clear();
        }

        if (recJets && jetveto) 
            for (auto& recJet: *recJets)
                (*jetveto)(recJet);

        if (metfilters) (*metfilters)(met, recEvt);

        if ((steering & DT::fill) == DT::fill) tOut->Fill();

        if (!isMC) continue;
        auto w = genEvt->weights.front();
        hSumWgt->Fill(0.0, w);
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    if (hSumWgt) hSumWgt->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get fractioned *n*-tuples after the n-tuplisation and merge them into a "
                            "single file. Complementary information about the pile-up may be included. "
                            "A minimal selection is applied, e.g. on the primary vertex (PV).",
                            DT::config | DT::split | DT::fill);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .arg<fs::path>("jetvetomap", "corrections.jetvetomap.filename", "ROOT file containing jet vet maps"
                                                                            "(`/dev/null` to deactivate)")
               .arg<bool>    ("METfilters", "corrections.METfilters", "activation boolean flag"        );

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::mergeNtuples(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
