#pragma once

#include <iostream>
#include <list>
#include <memory>
#include <optional>
#include <utility>

#include <Eigen/Dense>

#include <TString.h>
#include <TTreeReader.h>
#include <TUnfoldBinning.h>

#include "Core/Unfolding/interface/DistVariation.h"

class TH1;
class TH2;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Utility function to init an optional TTreeReaderArray/TTreeReaderValue.
/// Example:
/// ~~~{.cpp}
/// struct A
/// {
///     std::optional<TTreeReaderValue<int>> value;
///     A(TTreeReader& reader)
///         : value(initOptionalBranch<decltype(value)>(reader, "branchName"))
///     {}
/// };
/// ~~~
///
/// \copyright
/// Originally written for the [Shears](https://gitlab.cern.ch/shears/shears)
/// framework.
template<class T>
auto initOptionalBranch(TTreeReader& reader, const char *name)
{
     return reader.GetTree()->GetBranch(name) != nullptr
         ? std::make_optional<typename T::value_type>(reader, name)
         : std::nullopt;
}

////////////////////////////////////////////////////////////////////////////////
/// Fills histograms for an observable
///
/// Idea:
/// - use pointers to this abstract class to define observables in daughter classes
/// - the fill functions are generic, and should be implemented in daughter classes
/// Goal: factorise the filling of the input histogram to the unfolding in an
///       observable-agnostic way, hence avoiding multiple executables.
///
/// Subclasses should use TTreeReaderValue and TTreeReaderArray to read branches.
struct Filler {
    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor.
    virtual ~Filler () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// For a given variation, fills the detector level distribution, as well as 
    /// a temporary histogram (reset at each event) to calculate the covariance
    /// matrix later on. The bin IDs are returned to avoid looping over many empty
    /// bins.
    virtual std::list<int> fillRec (DistVariation&);

    ////////////////////////////////////////////////////////////////////////////////
    /// Implementation of matching algorithm.
    virtual void match ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Fill RM, gen, miss, fake histograms for a given variation.
    virtual void fillMC (DistVariation&);
};

////////////////////////////////////////////////////////////////////////////////
/// Transform a yield onto another observable such as a ratio or a fraction.
///
/// Gaussian uncertainties are assumed. Correlations are taken into account
/// and propagated to the new observable using Teddy.
///
/// Goal: factorise the filling of the input histogram to the transformation in
///       an observable-agnostic way, hence avoiding multiple executables.
class Transformer {

protected:

    //static int offset; //!< number of bins before the present distribution in original binning

    ////////////////////////////////////////////////////////////////////////////////
    /// Copy the axis of the source binning object into the target binning object.
    static void AddAxis (TUnfoldBinning *, //!< source binning
                         TUnfoldBinning *, //!< target binning
                         int //!< axis index (follow `TUnfoldBinning`'s convention)
                         );

public:

    static Eigen::VectorXd y; //!< output vector

    TUnfoldBinning * preBinning, //!< source binning (either rec or gen level)
                   * postBinning; //!< target binning

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor.
    virtual ~Transformer () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// Copies the input distribution by default.
    virtual void Transform
        (const Eigen::VectorXd& //!< event in original zero-suppressed flat binning
        ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Remove bins with too low coverage for Gaussian hypothesis
    virtual void RemoveBadInputBins (TH1 *, //!< distribution
                                     TH2 * //!< its covariance matrix
                                    );

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor.
    ///
    /// NOTE: subnodes are currently not supported (TODO?)
    Transformer (TUnfoldBinning *, //!< source binning
                 bool = true //!< flag to clone the original binning in target binning
                );
};

////////////////////////////////////////////////////////////////////////////////
/// Represents an observable that can be unfolded
///
/// Idea:
/// - use pointers to this abstract class to define observables in daughter classes
/// - this class handles everything that doesn't access event data (e.g. the
///   binning)
/// - it also gives access to a \ref Filler through \ref getFiller()
///
/// This class is used repeatedly by several executables related to the unfolding
/// (nearly all, to the notable exception of `unfold` itself), as well as in macros
/// used for plotting.
struct Observable {

    static bool isMC; //!< flag from metainfo
    // TODO specific to jet analyses
    static double maxDR; //!< max Delta R

    // The following objects may be used either directly in `getUnfHist` command
    TUnfoldBinning * recBinning, //!< detector-level binning
                   * genBinning; //!< particle-level binning

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable. By default, a dummy filler is returned.
    virtual std::unique_ptr<Filler> getFiller (TTreeReader& reader) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable. By default, no operation is applied.
    virtual std::unique_ptr<Transformer> getTransformer (TUnfoldBinning *) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Construct regularisation matrix
    virtual void setLmatrix (const std::unique_ptr<TH1>&, //!< bias
                             std::unique_ptr<TH2>& //!< (global) L matrix
                            );

    virtual ~Observable () = default;
protected:

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Observable (const char *, const char *);
};

////////////////////////////////////////////////////////////////////////////////
/// Get the observables to unfold
std::vector<Observable *> GetObservables // TODO: smart pointer?
                    (boost::property_tree::ptree //!< expect labels
                     );

} // end of namespace DAS::Unfolding
