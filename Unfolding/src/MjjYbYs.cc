#include "Core/CommonTools/interface/variables.h"
#include "Core/Unfolding/interface/MjjYbYs.h"

#include "Math/VectorUtil.h"

#include <colours.h>
#include <exceptions.h>

#include <TVectorT.h>

using namespace std;
using namespace DAS;
using namespace DAS::Unfolding;
using namespace DAS::Unfolding::DijetMass3D;
namespace DE = Darwin::Exceptions;


MjjYbYsFiller::MjjYbYsFiller (const MjjYbYs& obs, TTreeReader& reader)
    : obs(obs)
    , genJets(initOptionalBranch<decltype(genJets)>(reader, "genJets"))
    , recJets(reader, "recJets")
    , gEv(initOptionalBranch<decltype(gEv)>(reader, "genEvent"))
    , rEv(reader, "recEvent")
{
}

void MjjYbYsFiller::match ()
{
    matched.reset();

    auto match = [this](size_t i, size_t j) {
        const FourVector& g = genJets->At(i).p4,
                          r = recJets.At(j).p4;
        using ROOT::Math::VectorUtil::DeltaR;
        auto DR = DeltaR(g, r);
        //cout << g << '\t' << r << '\t' << DR << '\t' << result << '\n';
        return DR < obs.maxDR;
    };

    // matching (swapping leading and subleading is allowed)
    matched = genJets->GetSize() > 1 && recJets.GetSize() > 1
              && (   (match(0,0) && match(1,1))
                  || (match(0,1) && match(1,0)) );
}

namespace {

////////////////////////////////////////////////////////////////////////////////
/// Dijet selection
template<typename Jet>
DAS::Di<const Jet, const Jet> selection (const TTreeReaderArray<Jet>& jets,
                                         const Uncertainties::Variation& v)
{
    Di<const Jet, const Jet> dijet;
    if (jets.GetSize() < 2) return dijet;

    dijet = jets.At(0) + jets.At(1);

    if (dijet.first->CorrPt(v) < 100 || dijet.second->CorrPt(v) < 50 ||
        dijet.first->AbsRap() >= maxy || dijet.second->AbsRap() >= maxy)
        dijet.clear();

    return dijet;
}

////////////////////////////////////////////////////////////////////////////////
/// Find the bin number.
///
/// \return bin index (0 means out of PS, like `TUnfoldBinning::GetGlobalBinNumber`)
template<typename Jet>
double getBinNumber (const DAS::Di<const Jet, const Jet>& dijet,
                     const Uncertainties::Variation& v,
                     TUnfoldBinning * bng)
{
    if (!dijet) return 0;

    float mass = dijet.CorrP4(v).M(),
          yb = abs(dijet.Yboost()),
          ys = abs(dijet.Ystar());

    return bng->GetGlobalBinNumber(mass, yb, ys);
}

} // end of anonymous namespace

list<int> MjjYbYsFiller::fillRec (DistVariation& v)
{
    auto dijet = selection(recJets, v);
    if (!dijet) return {};

    int i = getBinNumber(dijet, v, obs.recBinning);
    if (i == 0) return {};

    double w = rEv->Weight(v);
    if (obs.isMC) w *= (*gEv)->Weight(v);
    w *= dijet.Weight(v);

    v.tmp->Fill(i, w);
    v.rec->Fill(i, w);

    return list<int>{i};
}

void MjjYbYsFiller::fillMC (DistVariation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto recdijet = selection(recJets, v);
    auto irec = getBinNumber(recdijet, v, obs.recBinning);
    double recW = rEv->Weight(v);
    if (irec > 0) recW *= recdijet.Weight(v);

    auto gendijet = selection(*genJets, v);
    auto igen = getBinNumber(gendijet, v, obs.genBinning);
    double genW = (*gEv)->Weight(v);
    if (igen > 0) genW *= gendijet.Weight(v);

    if (igen > 0) v.gen->Fill(igen, genW);

    if (*matched) {
        if      (irec > 0  && igen >  0) {    v.RM->Fill(igen, irec, genW *    recW );
                                     v.missNoMatch->Fill(igen,       genW * (1-recW)); }
        else if (irec == 0 && igen >  0) v.missOut->Fill(igen,       genW           );
        else if (irec > 0  && igen == 0) v.fakeOut->Fill(      irec, genW *    recW );
    }
    else {
        if (igen > 0) v.missNoMatch->Fill(igen, genW       );
        if (irec > 0) v.fakeNoMatch->Fill(irec, genW * recW);
    }
}

////////////////////////////////////////////////////////////////////////////////

MjjYbYs::MjjYbYs () :
    Observable(__FUNCTION__, "Dijet mass double differential cross section")
{
    vector<double> recMjj_edges;
    recMjj_edges.reserve(Mjj_edges.size()*2);

    // rec binning with twice more bins
    for (size_t j = 0; j < Mjj_edges.size()-1; ++j) {
        double m = Mjj_edges.at(j),
               M = Mjj_edges.at(j+1);
        recMjj_edges.push_back(m);
        recMjj_edges.push_back((m+M)/2);
    }
    recMjj_edges.push_back(Mjj_edges.back());

    int nRecMjjBins = recMjj_edges.size()-1;
    int nGenMjjBins =    Mjj_edges.size()-1;

    recBinning->AddAxis("Mjj", nRecMjjBins, recMjj_edges.data(),false,false);
    recBinning->AddAxis("yb" ,   nYbYsBins,      y_edges.data(),false,false);
    recBinning->AddAxis("ys" ,   nYbYsBins,      y_edges.data(),false,false);
    genBinning->AddAxis("Mjj", nGenMjjBins,    Mjj_edges.data(),false,false);
    genBinning->AddAxis("yb" ,   nYbYsBins,      y_edges.data(),false,false);
    genBinning->AddAxis("ys" ,   nYbYsBins,      y_edges.data(),false,false);
}

unique_ptr<DAS::Unfolding::Filler> MjjYbYs::getFiller (TTreeReader& reader) const
{
    return make_unique<MjjYbYsFiller>(*this, reader);
}

[[ deprecated ]]
void MjjYbYs::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    cout << "No L matrix for 3D Dijet mass" << endl;
}
