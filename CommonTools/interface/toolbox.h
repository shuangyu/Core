#pragma once

#include <vector>
#include <iostream>
#include <cassert>
#include <stdexcept>

#include <TKey.h>
#include <TList.h>
#include <TString.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TParameter.h>
#include <TDirectory.h>

#include "Math/PtEtaPhiM4D.h"
#include "Math/Point3D.h"
#include "Math/VectorUtil.h"

#include "Core/Objects/interface/Jet.h"

#include <boost/exception/all.hpp>

namespace DAS {

inline bool pt_sort (const RecJet& j1, const RecJet& j2)
{
    return j1.CorrPt() > j2.CorrPt();
}

////////////////////////////////////////////////////////////////////////////////
/// Check if branch exists
///
/// Loop over branches of `tree` and check if `brName` exists
template<typename TTreePtr> bool branchExists (const TTreePtr& tree, TString brName)
{
    auto brList = tree->GetListOfBranches();
    bool brFound = false;
    for (auto it = brList->begin(); it != brList->end(); ++it) {
        TString name = (*it)->GetName();
        if (name == brName) brFound = true;
    }
    return brFound;
}

////////////////////////////////////////////////////////////////////////////////
/// Find all objects of type `T` directly in a `TDirectory` (i.e. not recursive)
template<typename T> std::vector<T*> GetObjects (TDirectory * dir)
{
    if (!dir)
        BOOST_THROW_EXCEPTION( std::runtime_error("Trying to access inexistent `TDirectory`") );
    std::vector<T*> objects;
    for (const auto&& element: *(dir->GetListOfKeys())) {
        auto key = dynamic_cast<TKey*>(element);
        if (auto obj = dynamic_cast<T*>(key->ReadObj()); obj)
            objects.push_back(obj);
    }
    return objects;
}

////////////////////////////////////////////////////////////////////////////////
/// Get ((...)sub)subdirectory of `dir`.
inline TDirectory * GetDirectory (TDirectory * dir, const std::vector<const char *>& names)
{
    for (const char * name: names) {
        TDirectory * subdir = dir->GetDirectory(name);
        if (!subdir)
            BOOST_THROW_EXCEPTION( std::invalid_argument(
                    Form("`%s` could not be found in `%s`", name, dir->GetName())) );
        dir = subdir;
    }
    return dir;
}

} // end of DAS namespace
