#pragma once

#include "Core/Objects/interface/PhysicsObject.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenJet
struct GenJet : public PhysicsObject {
    static inline const char * const ScaleVar = "GenJetScales", //!< Name of jet gen scales in variations
                             * const WeightVar = "GenJetWgts";  //!< Name of jet gen weights in variations

    int nBHadrons = -1, //!< Number of *B* hadrons (0, 1 or 2+)
        nCHadrons = -1; //!< Number of *D* hadrons (0, 1+ in 2017; 0, 1, 2+ in 2016)
    int partonFlavour = 0; //!< Parton flavour (PDG ID)

    inline int hadronFlavour () const
    {
             if (nBHadrons > 0) return 5;
        else if (nCHadrons > 0) return 4;
        else                    return 0;
    }

private:
    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }
};

////////////////////////////////////////////////////////////////////////////////
/// class RecJet
///
/// Contains:
///  - HF discriminants,
///  - information for the application of the JES corrections
///  - the JEC factors after applicaton of the JES correctoon and JER smearing (additional entries to the vector include the systematic variations)
///  - the HF content (only relevant for simulation; trivial for data)
struct RecJet : public GenJet {
    static inline const char * const ScaleVar = "RecJetScales", //!< Name of jet rec scales in variations
                             * const WeightVar = "RecJetWgts"; //!< Name of jet rec weights in variations

    // for jet energy scale corrections
    float area = -1, //!< Jet area (should be peaked at pi*R^2), used for the JES corrections
          puID = -1; //!< pile-up jet ID

    ////////////////////////////////////////////////////////////////////////////////
    /// Contains the values describing the probability for the jet to be
    /// heavy-flavoured.
    struct Tagger {
        float probc = -1,    //!< Probability for the jet to contain at least one *D* hadron
              probb = -1,    //!< Probability for the jet to contain exactly one *B* hadron decaying hadronically
              probbb = -1,   //!< Probability for the jet to contain at least two *B* hadrons
              problepb = -1, //!< Probability for the jet to contain exactly one *B* hadron decaying leptonically
              probuds = -1,  //!< Probability for the jet to be a quark jet with no HF hadron
              probg = -1;    //!< Probability for the jet to be a gluon jet with no HF hadron

        inline float B    () const { return probb+probbb+problepb; }
        inline float CvsL () const { return probc/(probc+probuds+probg); }
        inline float CvsB () const { return probc/(probc+probb+probbb+problepb); }
    } DeepJet; //!< [Reference](https://iopscience.iop.org/article/10.1088/1748-0221/15/12/P12012/pdf)

    RecJet () = default; //!< Constructor (trivial)

private:
    std::string_view scale_group () const final { return ScaleVar; }
    std::string_view weight_group () const final { return WeightVar; }
};

} // end of DAS namespace

inline std::ostream& operator<< (std::ostream& s, const DAS::GenJet& jet)
{
    return s << jet.p4 << ' ' << jet.nCHadrons << ' ' << jet.nBHadrons;
}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenJet +;
#pragma link C++ class std::vector<DAS::GenJet> +;

#pragma link C++ class DAS::RecJet +;
#pragma link C++ class DAS::RecJet::Tagger +;
#pragma link C++ class std::vector<DAS::RecJet> +;
#endif
