#pragma once

#include <string>

namespace DAS {

/// Suffix used for "up" uncertainties. Follows the Combine convention.
inline const std::string SysUp   = "Up";
/// Suffix used for "down" uncertainties. Follows the Combine convention.
inline const std::string SysDown = "Down";

} // namespace DAS
