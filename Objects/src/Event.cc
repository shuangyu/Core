#include <stdexcept>

#include "Core/Objects/interface/Event.h"

#define DUMMY -999

using namespace DAS ;

void GenEvent::clear ()
{
    hard_scale = DUMMY;
    weights.clear();
}

void RecEvent::clear ()
{
    runNo = DUMMY; evtNo = DUMMY; lumi = DUMMY;
    weights.clear();
}

void Trigger::clear ()
{
    Bit.clear();
    PreHLT.clear();
    PreL1min.clear();
    PreL1max.clear();
}

void MET::clear ()
{
    Et = DUMMY; SumEt = DUMMY; Pt = DUMMY; Phi = DUMMY;
    Bit.clear();
}

// these are the recommended values for Run-2 at 13 TeV
float PileUp::MBxsec = 69200 /* mb */,
      PileUp::MBxsecRelUnc = 0.046; // 4.6%

float PileUp::GetTrPU (const char v) const
{
    if (trpu == DUMMY)
        throw std::runtime_error("True pile-up has not been set properly.");

    switch (v) {
        case '+': return trpu * (1+MBxsecRelUnc);
        case '-': return trpu * (1-MBxsecRelUnc);
        case '0':
        default:  return trpu;
    }
}

TRandom3 PileUp::r3(42);

float PileUp::GetInTimePU (const char v, bool useDefault) const 
{
    if (useDefault) {
        if (intpu == DUMMY)
            throw std::runtime_error("In-time pile-up has not been set properly.");
        switch (v) {
            case '+': return intpu * (1+MBxsecRelUnc);
            case '-': return intpu * (1-MBxsecRelUnc);
            case '0':
            default:  return intpu;
        }
    }
    else {
        if (trpu == DUMMY)
            throw std::runtime_error("True pile-up has not been set properly.");
        float trpu = GetTrPU(v);
        return r3.Poisson(trpu);
    }
}

void PileUp::clear ()
{
    nVtx = DUMMY; rho = DUMMY;
    trpu = DUMMY; intpu = DUMMY;
    pthats.clear();
    MBevents.clear();
}

void PrimaryVertex::clear ()
{
    Rho = DUMMY; z = DUMMY;
    ndof = DUMMY; chi2 = DUMMY;
    distGen = DUMMY;
}
