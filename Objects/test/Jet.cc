#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/Jet.h"

#define BOOST_TEST_MODULE Jet
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace DAS;
using namespace std;

BOOST_AUTO_TEST_CASE( genlevel )
{
    GenJet jet;
    BOOST_TEST( jet.scales.size() == 1 );
    BOOST_TEST( jet.weights.size() == 1 );
    BOOST_TEST( jet.nBHadrons == -1 );
    BOOST_TEST( jet.nCHadrons == -1 );
    BOOST_TEST( jet.partonFlavour == 0 );
    cout << jet << endl;
}

BOOST_AUTO_TEST_CASE( reclevel )
{
    RecJet jet;
    BOOST_TEST( jet.scales.size() == 1 );
    BOOST_TEST( jet.weights.size() == 1 );
    BOOST_TEST( jet.nBHadrons == -1 );
    BOOST_TEST( jet.nCHadrons == -1 );
    BOOST_TEST( jet.partonFlavour == 0 );
    BOOST_TEST( jet.area == -1 );
    BOOST_TEST( jet.puID == -1 );
    cout << jet << endl;
}

#endif
