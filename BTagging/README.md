# BTag Efficiency

_Goal_: provide efficiency and mistag rate and apply scale factors to jets of all flavours. The `BTagCalibration` code was taken from BTV.

The `applyBTagSF` command follows the [BTV recommendations](https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation): to calibrate the B-tagging, the scale factors provided centrally must be used with the efficiency obtained for the selection of the analysis. The `getBTagBinnedDiscriminant` command may be used to obtain the DeepJet discriminant in bins of the kinematics of the jets, for both data and MC. The `getBTagFraction` extracts the B-tagged jet fraction from its output whereas the `getBTagPerformance` extracts the efficiency and mistag rates; these two commands are only used to investigate and validate the B-tagging, as the `applyBTagSF` takes the output of `getBTagBinnedDiscriminant` directly too. In case of doubt, the command helpers indicate the necessary input.
