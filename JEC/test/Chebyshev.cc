#include <cstdlib>
#include "Core/JEC/interface/resolution.h"

#include <TROOT.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLegend.h>
#include <TF2.h>
#include <TH1.h>

int main (int argc, char * argv[])
{
    gROOT->SetBatch();
    new TCanvas("Chebyshev", "Chebyshev", 800, 600);

    double y1 = -0.4, 
           y2 = 0.4,
           x1 = 49,
           x2 = 2500;

    static const size_t N = 5; // order of Chebyshev polynomials
    TF2 * f = new TF2("GaussChebyshev", GaussChebyshev<N>(x1, x2), x1, x2, y1, y2, N+4);
    double p[] = { /* Chebyshev:        */ -0.00090106836, 0.0017638530, -0.00098910301, -0.00033273812, -0.00058063049, -0.00028576701,
                   /* resolution width: */ 2.46508, 1.66787, 0.00121153 };
    // -0.000901068
    // 0.00176385
    // -0.000989103
    // -0.000332738
    // -0.00058063
    // -0.000285767
    // 2.46508
    // 1.66787
    // 0.00121153
    f->SetTitle(";p_{T}^{gen}   [GeV];#Delta=#frac{p_{T}^{rec}-p_{T}^{gen}}{p_{T}^{gen}}");
    f->SetParameters(p);
    f->Draw("surf2");

    for (double pt: {100, 500, 1000, 2000})
        cout << "f(" << pt << ",0) = " << f->Eval(pt, 0) << endl;

    f->GetXaxis()->SetMoreLogLabels();
    f->GetXaxis()->SetNoExponent();

    //TF1 * fmu = new TF1("Chebyshev", Chebyshev<N>(x1,x2), x1, x2, N);
    //fmu->SetParameters(p);
    //fmu->SetLineColor(kOrange);
    //fmu->Draw("same");

    TLine * l = new TLine;
    l->SetLineStyle(kDashed);
    l->DrawLine(x1, 0, x2, 0);

    gPad->SetTicks(1,1);
    gPad->SetLogx();
    gPad->SetLogz();
    gPad->RedrawAxis();
    gPad->Print("GaussChebyshev.pdf");

    return EXIT_SUCCESS;
}
