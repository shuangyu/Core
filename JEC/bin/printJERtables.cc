#include <cstdlib>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TF1.h>
#include <TH3.h>

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"

#include "Math/VectorUtil.h"

#include "common.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

string getNSC (TDirectory * dir,
               float rhomin, float rhomax,
               float etamin, float etamax)
{
    auto fNSC = dynamic_cast<TF1*>( dir->Get("sigma") );
    stringstream ss;
    if (fNSC == nullptr) {
        cerr << red << "`sigma` could not be found in `" << dir->GetPath() << "`\n" << normal;
        return ss.str();
    }

    int Npars = fNSC->GetNpar();
    double * pars = new double[Npars];
    fNSC->GetParameters(pars);

    ss << setw(5) << etamin
       << setw(8) << etamax
       << setw(8) << rhomin
       << setw(8) << rhomax
       << setw(3) << 6 // 2 pt edges + 4 parameters
       << setw(5) << pt_JERC_edges.front()
       << setw(6) << pt_JERC_edges.back();
    for (int i = 0; i < Npars; ++i)
        ss << setw(15) << pars[i];
    ss << '\n';

    cout << ss.rdbuf();

    return ss.str();
}

////////////////////////////////////////////////////////////////////////////////
/// Create JER tables in JetMET format with DAS tools
void printJERtables
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output txt file
              int year)
{
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    assert(source != nullptr);

    if (fs::exists(output))
        cerr << red << "Overwriting " << output << '\n' << normal;
    ofstream file;
    file.open(output);

    // WARNING: make sure that this formula is the one used in `fitJetResolution`!
    const char * formula = "sqrt([0]*abs([0])/(x*x)+[1]*[1]/pow(x,[3])+[2]*[2])";
    file << "{2    JetEta    Rho    1    JetPt    " << formula << "    Resolution}" << endl;

    for (int irho = 0; irho < nRhoBins.at(year); ++irho)
    for (int ieta = 0; ieta < nAbsEtaBins; ++ieta) {
        auto dir = dynamic_cast<TDirectory*>( source->Get( Form("rhobin%d/Response_rhobin%d/etabin%d", irho+1, irho+1, ieta+1) ) );
        float rhomin = rho_edges.at(year).at(irho),
              rhomax = rho_edges.at(year).at(irho+1),
              etamin = abseta_edges.at(ieta),
              etamax = abseta_edges.at(ieta+1);

        file << getNSC(dir, rhomin, rhomax,  etamin,                         etamax); // Positive eta bins
        file << getNSC(dir, rhomin, rhomax, -etamax, etamin == 0 ? etamin : -etamin); // Negative eta bins (replicate information)
        cout << flush;
    }

    file.close();
    source->Close();

    cout << red << "\nWARNING: output table must be sorted before used!\n" << orange
         << "Table must start from first rho bin and display all corresponding eta bins\n"
         << "from negative to positive bins and then repeat for remaining rho bins.\n"
         << "One way to do this is to sort the table with vim:\n"
         << " - Open the txt table with vim.\n"
         << " - Select all rows, except the first one (which just contains names of columns), by pressing `" << red << "shift+v" << orange << "`.\n"
         << " - Press `" << red << ":" << orange << "` and execute the following command for selected rows `" << red << ":'<,'>!sort -k 3,3n -k 1,1n" << orange << "`,\n"
         << "   this sorts by 3rd column (-k 3) and treats values as numbers (3n),\n"
         << "   then sorts by 1st column (-k 1) and treats values as numbers (1n) while respecting previous sort.\n"
         << " - Lastly fix column identation by executing `" << red << ":'<,'>!column -t" << orange << "` in the same exact rows." << normal << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output year\n"
             << "where\tinput = root file produced by `fitJetResolution`\n"
             << "     \toutput = txt file containing resolution in JetMET table format (name suggestion `[tag]_MC_PtResolution_AK[cone size]PFchs.txt`)\n"
             << "     \tyear = 201*\n"
             << "Tag example      : `Summer19UL17_JRV42`\n"
             << "Cone size example: `4` or `8`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input  = argv[1],
             output = argv[2];
    int year = atoi(argv[3]);

    printJERtables(input, output, year);
    return EXIT_SUCCESS;
}
#endif

