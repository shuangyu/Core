#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TH1.h>
#include <TH3.h>

#include "common.h"
#include "Core/JEC/interface/matchingJM.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Create a TH3 histogram for the response with appropriate axes
unique_ptr<TH3> makeRespHist (TString name)
{
    static int nResBins = 200;
    static auto resBins = getBinning(nResBins, 0, 2);
    static const char * axistitles = ";p_{T}^{gen};|#eta|^{rec};#frac{p_{T}^{rec}}{p_{T}^{gen}}";

    // Filling in bins of ptgen results in better conditioned response distributions
    // Pure rec level weight is used
    auto h = make_unique<TH3D>(name, axistitles,
                nPtJERCbins, pt_JERC_edges.data(),
                nAbsEtaBins, abseta_edges.data(),
                nResBins, resBins.data());
    h->SetDirectory(nullptr);
    return h;
}

////////////////////////////////////////////////////////////////////////////////
/// Get JER differential resolution and balance.
void getJetResponse
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
         const fs::path& output, //!< name of output ROOT file (histograms)
         const pt::ptree& config, //!< config file from `DT::Options`
         const int steering, //!< steering parameters from `DT::Options`
         const DT::Slice slice = {1,0} //!< slices for running
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) 
        BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.", metainfo) );

    const auto R = metainfo.Get<int>("flags", "R");
    const float maxDR = R/10./2.;
    cout << "Radius for matching: " << maxDR << endl;

    RecEvent * rEv = nullptr;
    tIn->SetBranchAddress("recEvent", &rEv);
    const auto hiPU = config.get<bool>("skims.pileup");
    PileUp * pileUp = nullptr;
    if (hiPU)
        tIn->SetBranchAddress("pileup", &pileUp);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("genJets", &genjets);

    unique_ptr<TH3> inclResp = makeRespHist("inclusive"); // Response inclusive in rho
    vector<unique_ptr<TH3>> rhobinsResp, // Response in bins of rho
                            mubinsResp;  // Response in bins of mu (true pileup)

    const auto year = metainfo.Get<int>("flags", "year");
    if (hiPU) {
        rhobinsResp.reserve(nRhoBins.at(year));
         mubinsResp.reserve(nRhoBins.at(year));
        for (int rhobin = 1; rhobin <= nRhoBins.at(year); ++rhobin) {
            rhobinsResp.push_back( makeRespHist( Form("rhobin%d", rhobin) ) );
             mubinsResp.push_back( makeRespHist( Form( "mubin%d", rhobin) ) );
        }
    }

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        // Matching
        if (genjets->size() > 3) genjets->resize(3); // Keep only three leading gen jets
        map<float, pair<RecJet, GenJet>> goodPairs = match(*recjets, *genjets, maxDR);

        auto recWgt = rEv->weights.front();
        optional<size_t> irho, imu;
        if (hiPU) {
            static const size_t nrho = static_cast<size_t>(nRhoBins.at(year));
            for (irho = 0; pileUp->rho > rho_edges.at(year).at(*irho+1) && *irho < nrho; ++*irho);
            if (*irho >= nrho)
                BOOST_THROW_EXCEPTION( DE::AnomalousEvent(Form("irho = %lu > nRhoBins.at(%d) = %d",
                                                            *irho, year, nRhoBins.at(year)), tIn) );
            static const size_t imuMax = mubinsResp.size()-1;
            imu = static_cast<size_t>(pileUp->GetTrPU())/10;
            *imu = min(*imu,imuMax);
        }

        auto itStop = goodPairs.begin();
        advance(itStop, min(goodPairs.size(),3lu)); // Keep only three first pairs (with smallest DR)

        for (auto pp = goodPairs.begin(); pp != itStop; ++pp) {
            const auto& p = pp->second;
            auto ptrec = p. first.CorrPt();
            auto ptgen = p.second.p4.Pt();
            assert(ptgen > 0); //TODO remove?

            // Fill resolution from smearing
            auto response = ptrec/ptgen;
            auto etarec   = abs( p.first.p4.Eta() );

            inclResp->Fill(ptgen, etarec, response, recWgt);
            if (irho) rhobinsResp.at(*irho)->Fill(ptgen, etarec, response, recWgt);
            if (imu )  mubinsResp.at(*imu )->Fill(ptgen, etarec, response, recWgt);

             // TODO: with gen level weight too?
        } // End of for (pairs) loop
    } // End of while (event) loop

    // Creating directory hierarchy and saving histograms
    inclResp->SetDirectory(fOut.get());
    inclResp->SetTitle("Response (inclusive)");
    inclResp->Write("Response");
    if (hiPU)
    for (int irho = 0; irho < nRhoBins.at(year); ++irho) {

        // TODO: remove upper boundaries from titles of last bins

        TString title = Form("%.2f < #rho < %.2f", rho_edges.at(year).at(irho), rho_edges.at(year).at(irho+1));
        fOut->cd();
        TDirectory * d_rho = fOut->mkdir(Form("rhobin%d", irho+1), title);
        d_rho->cd();
        rhobinsResp.at(irho)->SetDirectory(fOut.get());
        rhobinsResp.at(irho)->SetTitle("Response (" + title + ")");
        rhobinsResp.at(irho)->Write("Response");

        title = Form("%d < #mu < %d", irho*10, (irho+1)*10);
        fOut->cd();
        TDirectory * d_mu = fOut->mkdir(Form("mubin%d", irho+1), title);
        d_mu->cd();
        mubinsResp.at(irho)->SetDirectory(fOut.get());
        mubinsResp.at(irho)->SetTitle("Response (" + title + ")");
        mubinsResp.at(irho)->Write("Response");
    }

    fOut->cd();
    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get response in bins of pt and eta.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<bool>("pileup", "skims.pileup", "flag to activate the binning in mu and rho");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getJetResponse(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
